import os
from datetime import date, datetime
from json import load
import pickle
from requests import get
import pandas as pd
from tqdm import tqdm


class CaseHandler:
    url_cases = 'https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson'
    filename_json = 'data/CoronaData.json'
    filename_pkl = 'data/CoronaData.pkl'

    case_data = None
    data_state = None

    possible_values = dict()

    def __init__(self):
        self.initData()

        # get possible values
        for scope in ['Bundesland', 'Landkreis', 'Geschlecht', 'Altersgruppe']:
            self.possible_values[scope] = sorted(self.case_data[scope].unique())

    def initData(self):
        # import case data
        today = datetime.timestamp(datetime.combine(date.today(), datetime.min.time()))
        vaccination_data_present = os.path.isfile(self.filename_pkl)
        if not vaccination_data_present or (os.path.getmtime(self.filename_pkl) < today):
            self.downloadData()
        else:
            data = pickle.load(open(self.filename_pkl, 'rb'))
            self.case_data = data['data']
            self.data_state = data['data_state']

    def downloadData(self, save=True):
        # make directory
        if not os.path.exists('data'):
            os.mkdir('data')

        # download data
        case_file = get(self.url_cases, allow_redirects=True, stream=True)
        filesize = int(case_file.headers.get('content-length'))
        block_size = 1024
        progress_bar = tqdm(total=filesize, unit='B', unit_scale=True, unit_divisor=block_size,
                            desc='Downloading Case Data')
        with open(self.filename_json, 'wb') as file:
            for data in case_file.iter_content(block_size):
                progress_bar.update(block_size)
                file.write(data)
        progress_bar.set_description(desc='Downloaded Case Data')
        progress_bar.close()

        if save:
            self.loadData()
            self.preprocessing()
            self.saveData()

    def loadData(self):
        print('Loading Data, please wait...')
        if os.path.isfile(self.filename_json):
            with open(self.filename_json) as json_file:
                data = load(json_file)
            print('... loaded')
            print('Normalizing...')
            self.case_data = pd.json_normalize(data["features"])
            print('... normalized')
            self.data_state = pd.to_datetime(self.case_data['properties.Datenstand'][0], format='%d.%m.%Y, %H:%M Uhr')
        else:
            raise Exception('Cases file has to be downloaded first')

    def preprocessing(self):
        print('Preprocessing...')
        # Rename Columns
        self.case_data.columns = self.case_data.columns.str.replace('properties.', '', regex=False)

        # Drop not needed columns
        self.case_data.drop(inplace=True, columns=['Datenstand', 'IstErkrankungsbeginn', 'Altersgruppe2', 'Refdatum'])

        # Parse dates
        self.case_data['Meldedatum'] = pd.to_datetime(self.case_data['Meldedatum'])
        self.case_data = self.case_data.set_index('Meldedatum')

        # Rename age groups
        self.case_data['Altersgruppe'] = self.case_data['Altersgruppe'] \
            .replace('[A]', '', regex=True) \
            .replace('[\+]', '-100', regex=True) \
            .replace('unbekannt', 'NA')

        print('... preprocessed')

    def saveData(self):
        print('Saving...')
        pickle.dump({'data': self.case_data,
                     'data_state': self.data_state},
                    open(self.filename_pkl, 'wb'))
        print('... saved')

    def getCaseData(self):
        return self.case_data

    def getDataState(self, string=False):
        if string:
            return self.data_state.strftime('%d.%m.%Y')
        else:
            return self.data_state

    def getPossibleValues(self, scope):
        if scope in self.possible_values.keys():
            return self.possible_values[scope]
        else:
            raise Exception('No possible values for this scope')

    def getScopes(self):
        return self.possible_values.keys()

