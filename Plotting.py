import plotly.express as px
import plotly.graph_objects as go

from numpy import arange

from CaseHandling import CaseHandler
from VaccinationHandling import VaccinationHandler
from InhabitantHandling import InhabitantHandler


class Plotter:
    color_dict = dict()
    cases = None
    vacc = None
    inh = None

    courseDict = {'Alle Fälle': 'AnzahlFall', 'Todesfälle': 'AnzahlTodesfall',
                  'Genesen': 'AnzahlGenesen'}

    def __init__(self, cases_handler: CaseHandler, vacc_handler: VaccinationHandler,
                 inhabitant_handler: InhabitantHandler):
        self.cases = cases_handler
        self.vacc = vacc_handler
        self.inh = inhabitant_handler
        self.setColors()

    # Set colors according to view mode
    def setColors(self, course='Alle Fälle'):
        colors = dict()

        # Set all colors for normal view
        if course == 'Alle Fälle':
            colors['Bundesland'] = px.colors.diverging.Spectral * 2
            colors['Landkreis'] = [None] * len(self.cases.getPossibleValues('Landkreis'))
            colors['Geschlecht'] = ['#3288BD', '#D53E4F', '#ABDDA4']
            colors['Altersgruppe'] = px.colors.diverging.Spectral

        # Set all colors for mortality view
        if course == 'Todesfälle':
            colors['Bundesland'] = ['#F4F4F2', '#E4E5E5', '#D5D7D8', '#C5C8CB', '#B6BABE', '#A6ABB1', '#979DA5',
                                    '#878E98', '#78808B', '#68717E', '#596371', '#495464'] * 2
            colors['Landkreis'] = [None] * len(self.cases.getPossibleValues('Landkreis'))
            colors['Geschlecht'] = ['#495464', '#D5D7D8', '#D1D1D1']
            colors['Altersgruppe'] = ['#F4F4F2', '#E4E5E5', '#D5D7D8', '#C5C8CB', '#B6BABE', '#A6ABB1',
                                      '#979DA5', '#878E98', '#78808B', '#68717E', '#596371', '#495464', '#3A3559']

        # Set all colors for recovered view
        if course == 'Genesen':
            colors['Bundesland'] = ['#fcde9c', '#FBC189', '#faa476', '#F58C72', '#f0746e', '#EA626F', '#e34f6f',
                                    '#E04473', '#dc3977', '#CB2F79', '#b9257a', '#9B2175', '#7c1d6f'] * 2
            colors['Landkreis'] = [None] * len(self.cases.getPossibleValues('Landkreis'))
            colors['Geschlecht'] = ['#42B7B9', '#eb4a40', '#fbe6c5']
            colors['Altersgruppe'] = px.colors.sequential.Emrld

        # Set all colors for vaccination
        colors['Impfung'] = ['#FCAF58', '#B1CF5F', '#E4E4E4']
        colors['Impfstoff'] = ['#66C29E', '#EE6377', '#EBC10F', '#C1321D', '#E4E4E4']
        colors['Ort'] = ['#D0D870', '#8BB4DE', '#E4E4E4']

        # Wrap colors in dictionary
        for handler in [self.cases, self.vacc]:
            for key in handler.getScopes():
                self.color_dict[key] = {value: colors[key][i] for i, value in enumerate(handler.getPossibleValues(key))}

    def getColor(self, scope, entity=None):
        if entity is None:
            return list(self.color_dict[scope].values())  # return values as a list

        if entity in self.color_dict[scope].keys():
            return self.color_dict[scope][entity]  # return single value
        else:
            raise Exception('Requested color is not in selected scope')

    # Draw method for case time graph
    def drawCaseTimeGraph(self, dataset, properties, course):
        figure = go.Figure(layout=go.Layout(autosize=True, margin=dict(l=0, r=0, b=30, t=0, pad=0), hovermode="closest",
                                            showlegend=False, paper_bgcolor='rgb(255,255,255)',
                                            plot_bgcolor='rgb(255,255,255)'))

        for i in self.cases.getPossibleValues(properties):
            plotData = dataset.loc[dataset[properties] == i]
            plotData = plotData.resample('D').sum()
            figure.add_trace(go.Bar(x=plotData.index, y=plotData[self.courseDict[course]], name=i,
                                    marker_color=self.getColor(properties, i)))
        # figure.update_xaxes(range=[dataset.index.max() - pd.Timedelta(90, 'days'), dataset.index.max()])
        figure.update_yaxes(gridcolor='#CDCDCD')
        figure.update_layout(barmode='stack')
        figure.update_layout(hovermode="x")

        return figure

    # Draw method for pie graphs
    def drawCasePie(self, dataset, properties, course):
        figure = go.Figure(layout=go.Layout(autosize=True, hovermode="closest", margin=dict(l=0, r=0, b=0, t=0, pad=0),
                                            showlegend=False))

        plotData = dataset[[properties, self.courseDict[course]]]
        plotData = plotData.groupby([properties]).sum()
        figure.add_pie(labels=plotData.index, values=plotData[self.courseDict[course]], hole=.5, sort=False)
        figure.update_traces(textinfo='label+percent', textfont_size=16, textposition='inside',
                             marker=dict(colors=self.getColor(properties)))
        figure.update_layout(uniformtext_minsize=12, uniformtext_mode='hide')

        return figure

    # Draw method for vaccination time graph
    def drawVaccinationTimechart(self, dataset, view='Impfung', cum=False):
        # Bar plot for vaccinations over time
        figure = go.Figure(layout=go.Layout(autosize=True, margin=dict(l=0, r=0, b=30, t=0, pad=0), hovermode="closest",
                                            showlegend=True, legend_x=0, legend_y=1,
                                            paper_bgcolor='rgb(255,255,255)', plot_bgcolor='rgb(255,255,255)'))

        for vaccination in self.vacc.getPossibleValues(view)[:-1]:
            figure.add_trace(go.Bar(x=dataset.index, y=dataset[vaccination + ('-Summe'*cum)],
                                    name=vaccination, marker_color=self.getColor(view, vaccination)))

        figure.update_yaxes(gridcolor='#CDCDCD')
        figure.update_layout(barmode='stack', hovermode="x")
        return figure

    # Draw method for vaccination barchart
    def drawVaccinationBarchart(self, dataset, view='Impfung', scope='country'):
        view = 'Impfung'  # due to the other data source, vaccine data is currently unavailable

        # Data handling
        inhabitants = self.inh.getEWZ(scope='Bundesländer')
        inhabitants['Total'] = inhabitants.sum()

        vaccinated = dict()
        vaccinated['Erstimpfung'] = dataset['Erstimpfung'].astype(int)
        vaccinated['Zweitimpfung'] = dataset['Zweitimpfung'].astype(int)
        vaccinated['BioNTech'] = None  # due to the other data source, this data is currently unavailable
        vaccinated['Moderna'] = None  # due to the other data source, this data is currently unavailable
        vaccinated['AstraZeneca'] = None  # due to the other data source, this data is currently unavailable
        vaccinated['nicht geimpft'] = inhabitants - vaccinated['Erstimpfung']

        quote = dict()
        quote['Erstimpfung'] = (vaccinated['Erstimpfung'] / inhabitants * 100).round(2)
        quote['Zweitimpfung'] = (vaccinated['Zweitimpfung'] / inhabitants * 100).round(2)
        quote['Erst-Zweitimpfung'] = quote['Erstimpfung'] - quote['Zweitimpfung']
        quote['Erst/Zweit'] = (vaccinated['Zweitimpfung'] / vaccinated['Erstimpfung'] * 100).round(2)
        # quote['BioNTech'] = (vaccinated['BioNTech'] / inhabitants * 100)
        # quote['Moderna'] = (vaccinated['Moderna'] / inhabitants * 100)
        # quote['AstraZeneca'] = (vaccinated['AstraZeneca'] / inhabitants * 100)
        quote['nicht geimpft'] = (vaccinated['nicht geimpft'] / inhabitants * 100)

        if scope == 'country':
            graph_range = dict(range=[0, 100])
            height = 150
            areas = ['Total']
            drop = self.cases.getPossibleValues('Bundesland')
        else:
            graph_range = dict(range=[0, round(max(quote['Erstimpfung']) + 0.49)])
            height = 800
            areas = sorted(self.cases.getPossibleValues('Bundesland'))
            drop = ['Total']

        figure = go.Figure(
            layout=go.Layout(autosize=True, height=height, margin=dict(l=170, r=0, b=30, t=0, pad=5),
                             hovermode="closest",
                             showlegend=False, paper_bgcolor='rgb(255,255,255)', xaxis=graph_range,
                             yaxis=dict(categoryorder='category descending'), plot_bgcolor='rgb(255,255,255)',
                             barmode='stack'))

        # Bar plot for whole country
        if view == 'Impfstoff':
            for vaccine in self.vacc.getPossibleValues('Impfstoff'):
                figure.add_trace(go.Bar(name=vaccine, x=quote[vaccine].drop(labels=drop), y=areas,
                                        orientation='h', marker={'color': self.getColor('Impfstoff', vaccine)},
                                        text=vaccinated[vaccine].drop(labels=drop)))
        else:
            for vaccination in self.vacc.getPossibleValues('Impfung'):
                value = quote[vaccination].drop(labels=drop)
                if vaccination == 'Erstimpfung':
                    value = quote['Erst-Zweitimpfung'].drop(labels=drop)
                figure.add_trace(go.Bar(name=vaccination, x=value, y=areas, orientation='h',
                                        marker={'color': self.getColor('Impfung', vaccination)},
                                        text=vaccinated[vaccination].drop(labels=drop)))

        for area in areas:
            figure.add_trace(go.Scatter(x=[50], y=[area],
                                        textfont=dict(size=12),
                                        text=[f"{vaccinated['Erstimpfung'][area]}/{inhabitants[area]} "
                                              f"({quote['Erstimpfung'][area]}%)"],
                                        mode="text"))
        # Add line for herd immunity
        figure.add_shape(type='line', yref='paper', y0=0, y1=1, xref='x', x0=60, x1=60, line=dict(color='#FF5464'))

        return figure

    # Draw function for vaccination counter (currently not used - does not work properly)
    def drawVaccinationCounter(self):
        data = {'progress': arange(0, 1.1, 1), 'y': [0] * 2}
        figure = px.bar(data, x='progress', y='y', range_x=[0, 1], animation_frame='progress', orientation='h')
        figure.update_traces(marker_color=self.getColor('Impfung', 'Erstimpfung'))
        figure.update_layout(transition={'duration': 10})
        return figure
