from VaccinationHandling import VaccinationHandler
from CaseHandling import CaseHandler
from InhabitantHandling import InhabitantHandler
import pandas as pd


class CovidCalculator:
    cases = None
    vacc = None
    inh = None

    case_buffer = None
    population_buffer = None
    course_buffer = None

    courseDict = {'Alle Fälle': 'AnzahlFall', 'Todesfälle': 'AnzahlTodesfall',
                  'Genesen': 'AnzahlGenesen'}

    def __init__(self, case_handler: CaseHandler, vaccination_handler: VaccinationHandler,
                 inhabitant_handler: InhabitantHandler):
        # load data
        self.cases = case_handler
        self.vacc = vaccination_handler
        self.inh = inhabitant_handler

        self.case_buffer = self.cases.getCaseData()
        self.population_buffer = self.inh.getPopulationData()
        self.course_buffer = 'Alle Fälle'

    def filterCasesByLocation(self, scope, entity=None, clear=False):
        if clear:
            self.clearCaseBuffer()

        if scope == "Bundesländer" and entity is not None and entity != []:
            self.case_buffer = self.case_buffer.loc[self.case_buffer['Bundesland'].isin(entity)]
        elif scope == "Landkreise" and entity is not None and entity != []:
            self.case_buffer = self.case_buffer.loc[self.case_buffer['Landkreis'].isin(entity)]

        self.population_buffer = self.inh.getPopulationData(scope, entity)

    def filterCasesByGender(self, gender, clear=False):
        if clear:
            self.clearCaseBuffer()

        self.case_buffer = self.case_buffer.loc[self.case_buffer['Geschlecht'].isin(gender)]

    def filterCasesByAge(self, age, clear=False):
        if clear:
            self.clearCaseBuffer()

        alter = self.cases.getPossibleValues('Altersgruppe')[age[0]:age[1]]
        self.case_buffer = self.case_buffer.loc[self.case_buffer['Altersgruppe'].isin(alter)]

    def filterCasesByDate(self, time_start, time_end, clear=False):
        if clear:
            self.clearCaseBuffer()

        self.case_buffer = self.case_buffer.loc[
            (self.case_buffer.index > time_start) & (self.case_buffer.index <= time_end)]

    def setCourse(self, course):
        self.course_buffer = course

    def getNumberOfCases(self, last_days=None):
        case_data = self.case_buffer

        if last_days is not None:
            # use utc = True if they change the date format again
            if (self.case_buffer.index[0].tzinfo is None or
                    self.case_buffer.index[0].tzinfo.utcoffset(self.case_buffer.index[0]) is None):
                utc = False
            else:
                utc = True

            case_data = case_data.loc[
                (case_data.index > (pd.to_datetime("today", utc=utc) - pd.Timedelta(last_days + 1, 'days'))) & (
                            case_data.index <= pd.to_datetime("today", utc=utc))]

        return case_data[self.courseDict[self.course_buffer]].sum()

    def getIncidence(self, last_days=None):
        n_cases = self.getNumberOfCases(last_days)
        return (n_cases * 100000) / (self.population_buffer['properties.EWZ'].sum())

    def getIncidenceTendency(self):
        dataset = self.case_buffer.resample('D').sum()
        last_fourteen_days = dataset.tail(15).index[:-1]
        tendency = 0
        for i, day in enumerate(last_fourteen_days[:7]):
            if dataset.at[day, self.courseDict[self.course_buffer]] != 0:
                tendency += (dataset.at[last_fourteen_days[i + 7], self.courseDict[self.course_buffer]] /
                             dataset.at[day, self.courseDict[self.course_buffer]]) / 7
        return tendency

    def clearCaseBuffer(self):
        self.case_buffer = self.cases.getCaseData()
        self.population_buffer = self.inh.getPopulationData()
        self.course_buffer = 'Alle Fälle'

    def getCurrentCaseBuffer(self, clear=False):
        buffer = self.case_buffer
        if clear:
            self.clearCaseBuffer()
        return buffer

    def getVaccinationsBl(self):
        return self.vacc.getBlData()

    def getVaccinationsTime(self):
        return self.vacc.getTimeData()

    def getPercentageVaccinated(self, scope='Deutschland', second=False):
        inhabitants = self.inh.getEWZ(scope)

        if scope == 'Deutschland':
            number_vacc = self.vacc.getNumberVaccinated(second=second)
        else:
            if not second:
                number_vacc = self.vacc.getBlData()['Erstimpfung']
            else:
                number_vacc = self.vacc.getBlData()['Zweitimpfung']

        return number_vacc / inhabitants * 100

    def getVaccRatioFirstSecond(self, scope='Deutschland'):
        if scope == 'Deutschland':
            number_first_vacc = self.vacc.getNumberVaccinated(second=False)
            number_second_vacc = self.vacc.getNumberVaccinated(second=True)
        else:
            number_first_vacc = self.vacc.getBlData()['Erstimpfung']
            number_second_vacc = self.vacc.getBlData()['Zweitimpfung']

        return number_second_vacc / number_first_vacc * 100