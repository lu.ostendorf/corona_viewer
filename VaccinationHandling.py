import os
import pickle
from datetime import date, datetime
from requests import get
import pandas as pd
from tqdm import tqdm


class VaccinationHandler:
    url_vaccinations_time = 'https://impfdashboard.de/static/data/germany_vaccinations_timeseries_v2.tsv'
    url_vaccinations_bl = 'https://impfdashboard.de/static/data/germany_vaccinations_by_state.tsv'
    url_vaccinations_deliveries = 'https://impfdashboard.de/static/data/germany_deliveries_timeseries_v2.tsv'
    filename_time_tsv = 'data/VaccinationDataTime.tsv'
    filename_bl_tsv = 'data/VaccinationDataBL.tsv'
    filename_deliver_tsv = 'data/VaccinationDataDelivered.tsv'
    filename_pkl = 'data/VaccinationData.pkl'

    vaccination_data_time = None
    vaccination_data_bl = None
    vaccination_data_deliver = None
    data_state = None

    possible_values = {'Impfung': ['Erstimpfung', 'Zweitimpfung', 'nicht geimpft'],
                       'Impfstoff': ['BioNTech', 'Moderna', 'AstraZeneca', 'Johnson&Johnson', 'nicht geimpft'],
                       'Ort': ['Impfzentrum', 'Arztpraxis', 'nicht geimpft']}

    time_rename_col_dict = {'dosen_erst_differenz_zum_vortag': 'Erstimpfung',
                            'dosen_zweit_differenz_zum_vortag': 'Zweitimpfung',
                            'personen_erst_kumulativ': 'Erstimpfung-Summe',
                            'personen_voll_kumulativ': 'Zweitimpfung-Summe',
                            'dosen_biontech_kumulativ': 'BioNTech-Summe',
                            'dosen_moderna_kumulativ': 'Moderna-Summe',
                            'dosen_astrazeneca_kumulativ': 'AstraZeneca-Summe',
                            'dosen_johnson_kumulativ': 'Johnson&Johnson-Summe',
                            'dosen_dim_kumulativ': 'Impfzentrum-Summe',
                            'dosen_kbv_kumulativ': 'Arztpraxis-Summe'}

    bl_rename_col_dict = {'peopleFirstTotal': 'Erstimpfung',
                          'peopleFullTotal': 'Zweitimpfung',
                          'vaccinationsTotal': 'Gesamtdosen'}

    bl_rename_row_dict = {'DE-BW': 'Baden-Württemberg', 'DE-BY': 'Bayern', 'DE-BE': 'Berlin',
                          'DE-BB': 'Brandenburg', 'DE-HB': 'Bremen', 'DE-HH': 'Hamburg',
                          'DE-HE': 'Hessen', 'DE-MV': 'Mecklenburg-Vorpommern', 'DE-NI': 'Niedersachsen',
                          'DE-NW': 'Nordrhein-Westfalen', 'DE-RP': 'Rheinland-Pfalz', 'DE-SL': 'Saarland',
                          'DE-SN': 'Sachsen', 'DE-ST': 'Sachsen-Anhalt', 'DE-SH': 'Schleswig-Holstein',
                          'DE-TH': 'Thüringen'}

    def __init__(self):
        self.initData()

    def initData(self):
        # import vaccination data
        today = datetime.timestamp(datetime.combine(date.today(), datetime.min.time()))
        vaccination_data_present = os.path.isfile(self.filename_pkl)
        if not vaccination_data_present or (os.path.getmtime(self.filename_pkl) < today):
            self.downloadData()
        else:
            vaccination_data = pickle.load(open(self.filename_pkl, 'rb'))
            self.vaccination_data_time = vaccination_data['time']
            self.vaccination_data_bl = vaccination_data['location']
            self.vaccination_data_deliver = vaccination_data['deliveries']
            self.data_state = vaccination_data['data_state']

    def downloadData(self, save=True):
        # make directory
        if not os.path.exists('data'):
            os.mkdir('data')

        # get request time data and bl data
        vaccination_time_file = get(self.url_vaccinations_time, allow_redirects=True)
        filesize_time = int(vaccination_time_file.headers.get('content-length'))

        vaccination_bl_file = get(self.url_vaccinations_bl, allow_redirects=True)
        filesize_bl = int(vaccination_bl_file.headers.get('content-length'))

        vaccination_deliver_file = get(self.url_vaccinations_deliveries, allow_redirects=True)
        filesize_deliver = int(vaccination_deliver_file.headers.get('content-length'))

        # make progress bar
        block_size = 1024
        progress_bar = tqdm(total=filesize_time + filesize_bl + filesize_deliver, unit='B', unit_scale=True,
                            unit_divisor=block_size, desc='Downloading Vaccination Data')

        with open(self.filename_time_tsv, 'wb') as file:
            for data in vaccination_time_file.iter_content(block_size):
                progress_bar.update(block_size)
                file.write(data)

        with open(self.filename_bl_tsv, 'wb') as file:
            for data in vaccination_bl_file.iter_content(block_size):
                progress_bar.update(block_size)
                file.write(data)

        with open(self.filename_deliver_tsv, 'wb') as file:
            for data in vaccination_deliver_file.iter_content(block_size):
                progress_bar.update(block_size)
                file.write(data)

        progress_bar.set_description(desc='Downloaded Vaccination Data')
        progress_bar.close()

        if save:
            self.loadData()
            self.preprocessing()
            self.saveData()

    def loadData(self):
        if os.path.isfile(self.filename_time_tsv) and os.path.isfile(self.filename_bl_tsv):
            self.vaccination_data_time = pd.read_csv(self.filename_time_tsv, sep='\t', index_col=0,
                                                     parse_dates=True)
            self.vaccination_data_bl = pd.read_csv(self.filename_bl_tsv, sep='\t', index_col=0,
                                                   parse_dates=True)
            self.vaccination_data_deliver = pd.read_csv(self.filename_deliver_tsv, sep='\t', index_col=0,
                                                        parse_dates=True)
            self.data_state = self.vaccination_data_time.index[-1]
        else:
            raise Exception('Vaccination files have to be downloaded first')

    def preprocessing(self):
        # Federal State Data
        # Rename Columns
        self.vaccination_data_bl.rename(columns=self.bl_rename_col_dict, inplace=True)
        # Rename Rows
        self.vaccination_data_bl.rename(index=self.bl_rename_row_dict, inplace=True)
        # Add Total Row
        self.vaccination_data_bl.loc["Total"] = self.vaccination_data_bl.sum()

        # Time Data
        # Rename Columns
        self.vaccination_data_time.rename(columns=self.time_rename_col_dict, inplace=True)
        # Get difference data for Vaccines
        for column in self.getPossibleValues('Impfstoff')[:-1] + self.getPossibleValues('Ort'):
            self.vaccination_data_time[column] = self.vaccination_data_time[f'{column}-Summe'].diff()

    def saveData(self):
        pickle.dump({'location': self.vaccination_data_bl,
                     'time': self.vaccination_data_time,
                     'deliveries': self.vaccination_data_deliver,
                     'data_state': self.data_state},
                    open(self.filename_pkl, 'wb'))

    def getTimeData(self):
        return self.vaccination_data_time

    def getBlData(self):
        return self.vaccination_data_bl

    def getDeliverData(self):
        return self.vaccination_data_deliver

    def getDataState(self, string=False):
        if string:
            return self.data_state.strftime('%d.%m.%Y')
        else:
            return self.data_state

    def getPossibleValues(self, scope):
        if scope in self.possible_values.keys():
            return self.possible_values[scope]
        else:
            raise Exception('No possible values for this scope')

    def getNumberVaccinated(self, second=False):
        if not second:
            return self.vaccination_data_bl['Erstimpfung'][-1]
        else:
            return self.vaccination_data_bl['Zweitimpfung'][-1]

    def getScopes(self):
        return self.possible_values.keys()
