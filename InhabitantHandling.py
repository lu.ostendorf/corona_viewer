import pandas as pd


class InhabitantHandler:
    population_file = "data/Einwohner.pkl"
    population_data = None

    def __init__(self):
        self.loadData()

    def loadData(self):
        self.population_data = pd.read_pickle(self.population_file)

    def getPopulationData(self, scope='All', entity=None):
        if scope == "Bundesländer" and entity is not None and entity != []:
            return self.population_data.loc[self.population_data['properties.BL'].isin(entity)]
        elif scope == "Landkreise" and entity is not None and entity != []:
            return self.population_data.loc[self.population_data['properties.county'].isin(entity)]
        else:
            return self.population_data

    def getEWZ(self, scope='Bundesländer'):
        grouped = self.population_data.groupby(['properties.BL']).sum()['properties.EWZ']
        if scope == 'Bundesländer':
            return grouped
        else:
            return grouped.sum()
