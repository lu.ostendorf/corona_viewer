import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from datetime import date

from VaccinationHandling import VaccinationHandler
from CaseHandling import CaseHandler
from InhabitantHandling import InhabitantHandler
from CovidCalculation import CovidCalculator
from Plotting import Plotter

# load data
cases = CaseHandler()
vacc = VaccinationHandler()
inhab = InhabitantHandler()

# initialize plotter and calculator
calc = CovidCalculator(cases, vacc, inhab)
plot = Plotter(cases, vacc, inhab)

# Dash Handle
app = dash.Dash(__name__,
                meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}])
server = app.server
app.config.suppress_callback_exceptions = True


# Define callback methods

# Callback for plotting case graphs
@app.callback(
    [Output('time_chart', 'figure'),
     Output('gender_pie', 'figure'),
     Output('age_pie', 'figure'),
     Output('extended_pie', 'figure'),
     Output('Fallzahl', 'children'),
     Output('Inzidenz', 'children'),
     Output('Kennzahl', 'children'),
     Output('arrow', 'src')],
    [Input('gender-dropdown', 'value'),
     Input('age-slider', 'value'),
     Input('course-select', 'value'),
     Input('date-picker', 'start_date'),
     Input('date-picker', 'end_date'),
     Input('date_checkbox', 'value'),
     Input('location-select', 'value'),
     Input('bl-select', 'value'),
     Input('lk-select', 'value')], prevent_initial_call=True)
def updateCaseGraphs(gender, age, course, time_start, time_end, enable_date_filter, location, bundesland, landkreis):

    calc.clearCaseBuffer()

    # Filter by location
    if location == "Bundesländer":
        calc.filterCasesByLocation(location, bundesland)
    elif location == "Landkreise":
        calc.filterCasesByLocation(location, landkreis)

    # Filter by gender
    calc.filterCasesByGender(gender)

    # Filter by age
    calc.filterCasesByAge(age)

    # Filter by date
    if enable_date_filter == ['true']:
        calc.filterCasesByDate(time_start, time_end)

    # Set Course
    calc.setCourse(course)
    plot.setColors(course)  # Set Colors

    # plot graphs
    cases_data = calc.getCurrentCaseBuffer()
    fig = plot.drawCaseTimeGraph(cases_data, "Geschlecht", course)
    pie1 = plot.drawCasePie(cases_data, "Geschlecht", course)
    pie2 = plot.drawCasePie(cases_data, "Altersgruppe", course)
    pie3 = plot.drawCasePie(cases_data, "Bundesland", course)

    # calculate indicators
    n_cases = calc.getNumberOfCases()
    cases_per_100k = round(calc.getIncidence(), 2)
    seven_day_incidence = round(calc.getIncidence(7), 2)

    # get incidence tendency
    tendency = calc.getIncidenceTendency()

    if tendency > 1:
        picture = app.get_asset_url("red_arrow.png")
    else:
        picture = app.get_asset_url("green_arrow.png")

    return fig, pie1, pie2, pie3, f'{n_cases:,}', seven_day_incidence, cases_per_100k, picture


# Callback for date filtering checkbox
@app.callback(
    Output('date-picker', 'disabled'),
    [Input('date_checkbox', 'value')], prevent_initial_call=True)
def disable_date(enable_date_filter):
    if enable_date_filter == ['true']:
        return False
    else:
        return True


# Callback for location selecting
@app.callback(
    [Output('bundesland_div', 'style'),
     Output('landkreis_div', 'style'),
     Output('bl-select', 'value'),
     Output('lk-select', 'value')],
    [Input('location-select', 'value')], prevent_initial_call=True)
def location_handling(location):
    if location == 'Bundesländer':
        return {"display": "block"}, {"display": "none"}, None, None
    elif location == 'Landkreise':
        return {"display": "none"}, {"display": "block"}, None, None
    else:
        return {"display": "none"}, {"display": "none"}, None, None


# Callback to update vaccination charts
@app.callback(
    [Output('vaccination_graph_ges', 'figure'),
     Output('vaccination_graph_bl', 'figure'),
     Output('vaccination_graph_time', 'figure'),
     Output('vaccination_graph_time_cum', 'figure')],
    [Input('view_select', 'value')], prevent_initial_call=True)
def updateVaccinationsGraphs(view):
    ges_figure = plot.drawVaccinationBarchart(calc.getVaccinationsBl(), view=view, scope='country')
    bl_figure = plot.drawVaccinationBarchart(calc.getVaccinationsBl(), view=view, scope='county')
    time_figure = plot.drawVaccinationTimechart(calc.getVaccinationsTime(), view=view)
    time_cum_figure = plot.drawVaccinationTimechart(calc.getVaccinationsTime(), view=view, cum=True)

    return ges_figure, bl_figure, time_figure, time_cum_figure


# Callback for View Selector
@app.callback(
    Output('right-column-vaccinations', 'children'),
    [Input('scope_select', 'value'),
     Input('view_select', 'value')], prevent_initial_call=True)
def updateVaccinationView(scope, view):
    content = vaccinationView(scope, view)
    return content


# Helper Callback for View Selector
@app.callback(
    Output('view_control', 'style'),
    [Input('scope_select', 'value')], prevent_initial_call=True)
def view_handling_vacc(view):
    if view != 'overall':
        return {"display": "none"}
    else:
        return {"display": "block"}


# Filling functions for html
def controlPanel(tab='cases'):
    """
        :return: A Div containing the controls
        """
    if tab == 'cases':
        content = [description(tab), case_controls()]
    elif tab == 'vaccinations':
        content = [description(tab), vaccination_controls()]
    else:
        content = []

    return html.Div(id=f"left-column-{tab}",
                    className="two columns",
                    children=content
                    )


def description(tab='cases'):
    """
    :return: A Div containing dashboard title & descriptions.
    """
    content = ''
    if tab == 'cases':
        content = "Wählen Sie die gewünschten Kriterien aus um deinen Graphen zu den aktuellen Corona Fallzahlen in " \
                  "Deutschland zu erhalten"
    elif tab == 'vaccinations':
        content = "Rechts sehen Sie eine Übersicht über die bisher durchgeführten Corona Impfungen in Deutschland"
    return html.Div(id=f"description-card-{tab}",
                    children=[
                        html.Div(
                            id=f"intro-{tab}",
                            children=[content,
                                      html.P(),
                                      html.Hr()]
                        ),
                    ])


def case_controls():
    """
    :return: A Div containing controls for graphs.
    """

    location_control = html.Div(id="location_control",
                                children=[
                                    html.H4("Standort"),
                                    dcc.Dropdown(id="location-select",
                                                 options=[{"label": i, "value": i} for i in
                                                          ['Deutschland', 'Bundesländer', 'Landkreise']],
                                                 value="Deutschland"
                                                 ),
                                    html.Div(id="bundesland_div",
                                             children=[
                                                 html.Br(),
                                                 dcc.Dropdown(
                                                     id="bl-select",
                                                     options=[{"label": i, "value": i} for i in
                                                              cases.getPossibleValues('Bundesland')],
                                                     multi=True,
                                                     placeholder="Wähle Bundesland"
                                                 )],
                                             style={"display": "none"}
                                             ),
                                    html.Div(id="landkreis_div",
                                             children=[
                                                 html.Br(),
                                                 dcc.Dropdown(
                                                     id="lk-select",
                                                     options=[{"label": i, "value": i} for i in
                                                              cases.getPossibleValues('Landkreis')],
                                                     multi=True,
                                                     placeholder="Wähle Landkreis"
                                                 )],
                                             style={"display": "none"}
                                             )
                                ])

    gender_control = html.Div(id="gender_control",
                              children=[
                                  html.H4("Geschlecht"),
                                  dcc.Dropdown(id="gender-dropdown",
                                               options=[
                                                   {'label': 'Männlich', 'value': 'M'},
                                                   {'label': 'Weiblich', 'value': 'W'},
                                                   {'label': 'Unbekannt', 'value': 'unbekannt'}
                                               ],
                                               multi=True,
                                               value=["M", "W", "unbekannt"]
                                               )
                              ])

    age_control = html.Div(id="age_control",
                           children=[
                               html.H4("Alter"),
                               dcc.RangeSlider(id="age-slider",
                                               marks={
                                                   0: '0',
                                                   1: '5',
                                                   2: '15',
                                                   3: '35',
                                                   4: '60',
                                                   5: '80',
                                                   6: '100',
                                                   7: 'NA'
                                               },
                                               min=0,
                                               max=7,
                                               value=[0, 7],
                                               step=None,
                                               pushable=1
                                               ),
                               html.Br()
                           ])

    course_control = html.Div(id="course_control",
                              children=[
                                  html.H4("Krankheitsverlauf"),
                                  dcc.Dropdown(id="course-select",
                                               options=[{"label": i, "value": i} for i in
                                                        ['Alle Fälle', 'Todesfälle', 'Genesen']],
                                               value="Alle Fälle"
                                               )
                              ])

    date_control = html.Div(id="date_control",
                            children=[
                                html.H4("Zeitraum"),
                                dcc.DatePickerRange(id='date-picker',
                                                    start_date=date(2020, 1, 3),
                                                    min_date_allowed=date(2020, 1, 3),
                                                    display_format='DD.MM.YYYY',
                                                    max_date_allowed=date.today(),
                                                    end_date=date.today()
                                                    ),
                                html.Br(),
                                html.Br(),
                                dcc.Checklist(id="date_checkbox",
                                              options=[{'label': 'Zeitraum Filtern', 'value': 'true'}]
                                              )
                            ])

    data_state = html.Div(id="data_state_cases",
                          children=[
                              html.H6(f'Datenstand: {cases.getDataState(string=True)}')
                          ])

    return html.Div(id="control-card",
                    children=[
                        location_control,
                        html.Br(),
                        html.Hr(),
                        gender_control,
                        html.Br(),
                        html.Hr(),
                        age_control,
                        html.Br(),
                        html.Hr(),
                        course_control,
                        html.Br(),
                        html.Hr(),
                        date_control,
                        html.Br(),
                        html.Hr(),
                        data_state
                    ])


def dataView():
    """
    :return: A Div containing the data view
    """
    init_data = cases.getCaseData()
    time_init = plot.drawCaseTimeGraph(init_data, 'Geschlecht', 'Alle Fälle')
    pie1_init = plot.drawCasePie(init_data, 'Geschlecht', 'Alle Fälle')
    pie2_init = plot.drawCasePie(init_data, 'Altersgruppe', 'Alle Fälle')
    pie3_init = plot.drawCasePie(init_data, 'Bundesland', 'Alle Fälle')

    overall_card = html.Div(id="overall_card", className="thirdcard columns",
                            children=[
                                html.H4("Fallzahl Addiert"),
                                html.H1(id="Fallzahl", children=['Lädt...'])
                            ])

    inzidenz_card = html.Div(id="inzidenz_card", className="thirdcard columns",
                             children=[
                                 html.Div(id="inzidenz",
                                          children=[
                                              html.H4("Inzidenz"),
                                              html.H1(id="Inzidenz", children=['Lädt...'])
                                          ]),
                                 html.Div(id="arrowbox",
                                          children=[
                                              html.Img(id='arrow')
                                          ])
                             ])

    additional_card = html.Div(id="additional_card", className="thirdcard columns",
                               children=[
                                   html.H4("Fälle pro 100K Einwohner"),
                                   html.H1(id="Kennzahl", children=['Lädt...'])
                               ])

    time_chart = html.Div(id="time_chart_card", className="wholecard columns",
                          children=[
                              html.H4("Corona Fälle"),
                              html.Hr(),
                              dcc.Graph(id="time_chart", figure=time_init),
                          ])

    pie_chart_gender = html.Div(id="gender_card", className="thirdcard columns",
                                children=[
                                    html.H4("Geschlecht"),
                                    html.Hr(),
                                    dcc.Graph(id="gender_pie", figure=pie1_init),
                                ])

    pie_chart_age = html.Div(id="age_card", className="thirdcard columns",
                             children=[
                                 html.H4("Alter"),
                                 html.Hr(),
                                 dcc.Graph(id="age_pie", figure=pie2_init),
                             ])

    pie_chart_extended = html.Div(id="extended_card", className="thirdcard columns",
                                  children=[
                                      html.H4("Weitere Infos"),
                                      html.Hr(),
                                      dcc.Graph(id="extended_pie", figure=pie3_init),
                                  ])

    return html.Div(id="right-column-cases", className="ten columns",
                    children=[
                        # Info Cards
                        overall_card,
                        inzidenz_card,
                        additional_card,
                        # Time Chart Card
                        time_chart,
                        # Pie Chart Cards
                        pie_chart_gender,
                        pie_chart_age,
                        pie_chart_extended,
                    ])


def vaccination_controls():
    scope_control = html.Div(id="scope_control",
                             children=[
                                 html.H4("Ansicht"),
                                 dcc.Dropdown(id='scope_select',
                                              options=[{"label": 'Überblick', "value": 'overall'},
                                                       {"label": 'Bundeländer', "value": 'counties'}],
                                              value="overall"
                                              )
                             ])

    view_control = html.Div(id="view_control",
                            children=[
                                html.H4("Betrachtungsrahmen"),
                                dcc.Dropdown(id='view_select',
                                             options=[{"label": 'Erst-/Zweitimpfung', "value": 'Impfung'},
                                                      {"label": 'Impfstoff', "value": 'Impfstoff'},
                                                      {"label": 'Ort der Verabreichung', "value": 'Ort'}],
                                             value="Impfung"
                                             ),
                                html.Br(),
                                html.Hr(),
                            ], style={"display": "block"})

    data_state = html.Div(id="data_state_vaccinations",
                          children=[
                              html.H6(f'Datenstand: {vacc.getDataState(string=True)}')
                          ])
    return html.Div(id="vaccination_controls", className='wholecard', children=[
        scope_control,
        html.Br(),
        html.Hr(),
        view_control,
        data_state
    ])


def vaccinationView(view, scope):
    """
        :return: A Div containing the vaccination view
        """

    ges_figure = plot.drawVaccinationBarchart(vacc.getBlData(), 'Impfung', 'country')
    bl_figure = plot.drawVaccinationBarchart(vacc.getBlData(), 'Impfung', 'county')
    time_figure = plot.drawVaccinationTimechart(vacc.getTimeData(), scope)
    time_cum_figure = plot.drawVaccinationTimechart(vacc.getTimeData(), scope, cum=True)
    counter = plot.drawVaccinationCounter()

    number_vacc = vacc.getNumberVaccinated()
    perc_vacc = round(calc.getPercentageVaccinated(), 2)
    perc_first_second = round(calc.getVaccRatioFirstSecond(), 2)

    # Info Cards
    percentage_card = html.Div(id="vacc_percentage_card", className="thirdcard columns",
                               children=[
                                   html.H4("Prozentsatz Geimpfte Personen"),
                                   html.H1(id="vacc_percentage", children=[f'{perc_vacc}%'])
                               ])

    number_card = html.Div(id="vacc_number_card", className="thirdcard columns",
                           children=[
                               html.H4("Geimpfte Personen"),
                               html.H1(id="vacc_number", children=[f'{number_vacc:,}'])
                           ])

    first_second_card = html.Div(id="vacc_first_second_card", className="thirdcard columns",
                                 children=[
                                     html.H4("Prozentsatz Zweitimpfung pro Erstimpfung"),
                                     html.H1(id="vacc_first_second", children=[f'{perc_first_second}%'])
                                 ])

    whole_country = html.Div(id="vaccination_graph_card_ges", className="wholecard columns",
                             children=[
                                 html.H4('Deutschland'),
                                 html.Hr(),
                                 html.P(),
                                 dcc.Graph(id="vaccination_graph_ges", figure=ges_figure)
                             ])

    counties = html.Div(id="vaccination_graph_card_bl", className="wholecard columns",
                        children=[
                            html.H4('Bundesländer'),
                            html.Hr(),
                            html.P(),
                            dcc.Graph(id="vaccination_graph_bl", figure=bl_figure)
                        ])

    time_chart = html.Div(id="vaccination_graph_card_time", className="wholecard columns",
                          children=[
                              html.H4('Zeitverlauf'),
                              html.Hr(),
                              html.P(),
                              dcc.Graph(id="vaccination_graph_time", figure=time_figure)
                          ])

    time_cum_chart = html.Div(id="vaccination_graph_card_time_cum", className="wholecard columns",
                          children=[
                              html.H4('Zeitverlauf Kummuliert'),
                              html.Hr(),
                              html.P(),
                              dcc.Graph(id="vaccination_graph_time_cum", figure=time_cum_figure)
                          ])

    counter = html.Div(id="vaccination_card_counter", className="wholecard columns",
                       children=[
                           html.H4('Counter'),
                           html.Hr(),
                           html.P(),
                           dcc.Graph(id="vaccination_counter", figure=counter)
                       ])

    if view == 'counties':
        return [number_card,
                percentage_card,
                first_second_card,
                whole_country,
                counties]
    else:
        return [number_card,
                percentage_card,
                first_second_card,
                time_chart,
                time_cum_chart
                ]  # counter]


app.layout = html.Div(
    id="app-container",
    children=[
        # Banner
        html.Div(id="banner", className="banner",
                 children=[html.Img(src=app.get_asset_url("corona_logo.png")),
                           html.H2("Corona Viewer")],

                 ),
        dcc.Tabs(id="Tabs", children=[
            dcc.Tab(id="cases_tab", label="Fallzahlen", children=[
                # Left column
                controlPanel('cases'),
                # Right column
                dataView()
            ]),
            dcc.Tab(id="vaccination_tab", label="Impfungen", children=[
                # Left column
                controlPanel('vaccinations'),
                # Right column
                html.Div(id="right-column-vaccinations", className="ten columns",
                         children=vaccinationView('overall', 'Impfung'))
            ])
        ])
    ],
)

app.run_server(debug=True)
