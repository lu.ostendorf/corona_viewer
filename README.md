<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer">
    <img src="/assets/corona_logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Corona Viewer</h3>

  <div align="center">
    A Dash App that I wrote during the Covid-19 pandemics to get insights and visualize the data provided by the Robert Koch Institute about the infections in  germany.
    <br />
    <a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer">View Demo</a>
    ·
    <a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer/issues">Report Bug</a>
    ·
    <a href="hhttps://git.rwth-aachen.de/lu.ostendorf/corona_viewer/issues">Request Feature</a>
  </div>
</div>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#limitations">Limitations</a>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

As the COVID-19 pandemics reached germany, there was not much information about its spread. Even though a counter for the number of cases was quickly available, no visualization and breakdown of the data by social and age groups was published. However the raw Dataset is available by the "Robert Koch Institute" (RKI) since March.

Since I was interested in the data, I first developed a desktop app using Python, Pandas and AppJar (see corresponding repo). Over time, the dataset became more complex and could no longer be visualized adequately with AppJar. So I developed this dash app to be able to use interactive graphs. You can see a screenshot of the basic user Interface below.

<a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer">
  <img src="/screenshots/Screenshot_Corona_Viewer_cases.png" alt="screenshot" width="1000">
</a>

In December 2020, the vaccination campaign in Germany has started. Since then, an Excel file with the relevant vaccination data has been published every day. Although the format of the Excel file changes almost daily so far, I try to feed the app with the vaccination data and visualize it alongside the COVID-19 cases. You can see pictures of the UI below:

Overview Vaccinations Germany:
<a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer">
  <img src="/screenshots/Screenshot_Corona_Viewer_vacc_ger.png" alt="screenshot" width="1000">
</a>

<br>

Overview over the vaccination progress in the individual federal states:
<a href="https://git.rwth-aachen.de/lu.ostendorf/corona_viewer">
  <img src="/screenshots/Screenshot_Corona_Viewer_vacc_bl.png" alt="screenshot" width="1000">
</a>

<!-- LIMITATIONS -->
## Limitations

The ongoing waves of infections let the dataset grow every day. As of now (01.04.2021) the Dataset of the infected people has a size of 808MB. Since downloading and processing these amounts of data takes a lot of time, the application gets increasingly unresponsive. As there are plenty of official Websites to track the Corona pandemics today, I won't optimize the app to run faster.

Official Websites:
  - https://impfdashboard.de/
  - https://experience.arcgis.com/experience/478220a4c454480e823b17327b2bf1d4
